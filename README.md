# OpenML dataset: AutoMLSelectorMulticlass

https://www.openml.org/d/44200

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

A multiclass classification problem to predict the best AutoML tool to solve a given SL task.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44200) of an [OpenML dataset](https://www.openml.org/d/44200). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44200/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44200/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44200/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

